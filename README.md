# develop-web-apps

A container image for developing web apps with Node, Gulp, etc.

Build it like this:

```shell
docker build . --tag develop-web-apps:latest
```

Run it like this:

```shell
docker run \
    --tty \
    --interactive \
    --publish 80:9000 \
    --name website \
    --volume ~/Projects:/projects \
    develop-web-apps
```
