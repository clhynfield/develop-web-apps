FROM node:10-stretch

MAINTAINER Clayton Hynfield <clayton@hynfield.org>

RUN yarn global add gulp-cli && \
    yarn cache clean && \
    rm -rf /var/cache/* /tmp/*

RUN apt-get update && apt-get install -y \
    zsh \
    vim \
    man \
    less \
    tmux \
    certbot \
    direnv \
 && rm -rf /var/lib/apt/lists/*

RUN curl \
    --remote-name \
    --location \
    https://github.com/getantibody/antibody/releases/download/v4.1.0/antibody_4.1.0_linux_amd64.deb \
    && dpkg --install antibody_4.1.0_linux_amd64.deb

ENV LC_CTYPE C.UTF-8

RUN chsh --shell /usr/bin/zsh

RUN rm -rf ~ && git clone https://github.com/clhynfield/dotfiles.git ~

RUN mkdir /projects && ln -s /projects "$HOME/Projects"
VOLUME "/projects"
WORKDIR /projects

EXPOSE 9000

CMD [ "tmux", "new-session", "-A", "-D" ]
